pip install -r requirements.txt 
python manage.py migrate
python manage.py runserver
sudo docker run -p 5672:5672 --name peliculas rabbitmq:3
celery -A Peliculas worker --loglevel=INFO  
celery -A Peliculas beat -l info   



After executing these steps you will be able to observe in the django administrator that records were entered in the Movie model. 
You can also see the crud by entering the url http://127.0.0.1:8000/
