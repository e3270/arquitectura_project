from django.db import models


# Create your models here.

class Movie(models.Model):
    """
    this is the model that stores
    the information brought from the scraper
    """

    link = models.TextField(unique=True)
    name = models.CharField('name', max_length=250, unique=True)
    year = models.CharField('year', max_length=100)
    ranking = models.CharField('ranking', max_length=100)
