import requests
from bs4 import BeautifulSoup

from celery import shared_task

from movie.models import Movie


# @app.task(bind=True, name='scraper')
@shared_task
def scraper():
    """
    This function extracts information from a movie page and then saves it to the Movie model

    return: a list of objects
    """

    url = 'https://www.imdb.com/chart/top'
    result = requests.get(url).text
    htmldata = "".join(item.strip() for item in str(result).split("\n"))
    doc = BeautifulSoup(htmldata, "html.parser")

    tbody = doc.tbody
    trs = tbody.contents

    data_movies = []
    for tr in trs:
        content = tr.contents
        link = content[0].a.img.attrs['src']
        name = content[1].a.string
        year = content[1].span.string
        ranking = content[2].string

        data_movies.append({'link': link, 'name': name, 'year': year, 'ranking': ranking})

    for item in data_movies:
        Movie.objects.update_or_create(**item)

    return Movie.objects.all()
