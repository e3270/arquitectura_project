from rest_framework import viewsets
from movie.Seriealizers.Serializers import MovieSerializer
from movie.models import Movie


class MovieViewSet(viewsets.ModelViewSet):
    """
    this view automatically provides all the methods of the Movie crud.
    """
    queryset = Movie.objects.all().order_by('id')
    serializer_class = MovieSerializer




