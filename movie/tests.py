from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from movie.models import Movie

# Create your tests here.
from movie.tasks import scraper


class MovieTests(APITestCase):

    def test_list_movie(self):

        movie = Movie.objects.create(link='http://yoantesdeti.com', name='Yo Antes de Ti', year='2018', ranking='80')
        movie.save()
        response = self.client.get('/Movie/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Movie.objects.count(), 1)
        self.assertEqual(Movie.objects.get().name, 'Yo Antes de Ti')

    def test_create_movie(self):
        """
        Ensure we can create a new account object.
        """

        data = {'link': 'http://rambo.com', 'name': 'Rambo', 'year': '1800', 'ranking': '80'}

        response = self.client.post('/Movie/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Movie.objects.count(), 1)
        self.assertEqual(Movie.objects.get().name, 'Rambo')

    def test_update_movie(self):
        """
        Ensure we can update a new account object.
        """
        movie = Movie.objects.create(link='http://yoantesdeti.com', name='Yo Antes de Ti', year='2018', ranking='80')
        movie.save()

        data = {'link': 'http://encanto.com', 'name': 'encanto', 'year': '2021', 'ranking': '45'}
        response = self.client.put('/Movie/1/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Movie.objects.count(), 1)
        self.assertEqual(Movie.objects.get(id=1).name, 'encanto')

    def test_delete_movie(self):
        """
        This test for delete movie
        """
        movie = Movie.objects.create(link='http://yoantesdeti.com', name='Yo Antes de Ti', year='2018', ranking='80') #creamos el objeto de prueba
        movie.save() #guardamos el objeto de prueba

        response = self.client.delete('/Movie/1/', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT) #Comparamos si el estado de la respuesta es igual
        self.assertEqual(Movie.objects.count(), 0) #al realizar la consulta se compara si hay un objeto

    def test_retrieve_movie(self):
        """
        Ensure we can retrieve a new account object.
        """

        movie = Movie.objects.create(link='http://yoantesdeti.com', name='Yo Antes de Ti', year='2018', ranking='80')
        response = self.client.get('/Movie/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Movie.objects.count(), 1)
        self.assertEqual(Movie.objects.get().name, 'Yo Antes de Ti')

    def test_save_scrapper(self):

        task = scraper.apply()
        result = task.get()
        self.assertEqual(len(result), 250)











