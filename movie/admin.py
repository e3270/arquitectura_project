from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin

from .models import Movie


class MovieAdmin(admin.ModelAdmin):
    """
    """
    list_display = (
        'id',
        'name',
        'ranking',
        'year',

    )


admin.site.register(Movie, MovieAdmin)
