# Generated by Django 4.0.4 on 2022-04-26 02:26

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Audiencia',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('audiencia', models.CharField(max_length=100, verbose_name='Audiencia')),
            ],
        ),
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link', models.CharField(max_length=100, verbose_name='link')),
                ('name', models.CharField(max_length=250, verbose_name='name')),
                ('year', models.CharField(max_length=100, verbose_name='year')),
                ('ranking', models.CharField(max_length=100, verbose_name='ranking')),
            ],
        ),
    ]
